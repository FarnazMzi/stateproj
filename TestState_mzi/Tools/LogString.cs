﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace TestState_mzi.Tools
{
    public static class LogString 
    {

        public static string ReturnLog(ISession iSession ,string action)
        {
            var userName = iSession.GetString("UserName");
            var log = $"انجام داد {action} عملیات Land برروی جدول  ({DateTime.Now.Date}) در تاریخ {userName} کاربر";
            return log;
        }
    }
}
