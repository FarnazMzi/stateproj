﻿jQuery.extend(jQuery.validator.messages, {
   
    required: "مقدار را وارد کنید.",
    remote: "مقدار وارد شده اشتباه است.",
    email: "ایمیل وارد شده اشتباه است.",
    url: "آدرس وارد شده اشتباه است.",
    date: "تاریخ وارد شده اشتباه است.",
    dateISO: "تاریخ وارد شده اشتباه است.",
    number: "مقدار وارد شده باید عدد باشد.",
    digits: "مقدار وارد شده باید یک عدد یک رقمی باشد.",
    creditcard: "شماره کارت اعتباری اشتباه است.",
    equalTo: "مقدار مشابه را دوباره وارد کنید.",
    maxlength: $.validator.format("طول مقدار وارد شده نباید بیشتر از {0} باشد."),
    minlength: $.validator.format("طول مقدار وارد شده باید حداقل برابر با {0} باشد."),
    rangelength: $.validator.format("طول مقدار وارد شده باید بین  {0} و {1} باشد."),
    range: $.validator.format("مقدار وارد شده باید بین  {0} و {1} باشد."),
    max: $.validator.format("مقدار وارد شده نباید بیشتر از {0} باشد."),
    min: $.validator.format("مقدار وارد شده باید حداقل برابر با {0} باشد."),
    regexNumber: $.validator.format("مقدار وارد شده باید عدد باشد"),
    regexFarsi: $.validator.format("مقدار وارد شده باید بصورت فارسی وارد شود"),
    regexEnglish: $.validator.format("مقدار وارد شده باید بصورت انگلیسی وارد شود"),
    regexPhone: $.validator.format("تلفن وارد شده صحیح نمیباشد"),
    regexMobile: $.validator.format("شماره موبایل وارد شده صحیح نمیباشد"),
    regexPassport: $.validator.format("اطلاعات پاسپورت اشتباه است"),
    regxDateFormat: $.validator.format("تاریخ وارد شده اشتباه است"),
    regexLetters: $.validator.format("مقدار وارد شده باید متن باشد"),
    regxGeneralDate: $.validator.format("تاریخ وارد شده صحیح نیست"),
    regexFarsiDesc: $.validator.format("متن وارد شده باید فارسی باشد"),
    regexPostalcode: $.validator.format("کد پستی صحیح نمیباشد"),
    regxNationalcode: $.validator.format("کد ملی صحیح نمیباشد."),
    regexGeographicalCode: $.validator.format("کد جغرافیای باید 12 رقم باشد"),
    regexFloorNo: $.validator.format("طبقه واحد ساختمان نمیتواند از 120 بیشتر"),
    regexPassword: $.validator.format("رمز عبور میباست شامل حروف انگلیسی، عدد و یکی از کاراکترهای %,^,&,... باشد"),
    regexPercent: $.validator.format("درصد میبایست عددی بین -۱۰۰ تا ۱۰۰ باشد."),
    regexBirthDay: $.validator.format("تاریخ تولد نمیتواند زیر 18سال باشد")
});

function checkNationalCode(code) {
    var L = code.length;
    if (L != 10) return false;
    if (L < 8 || parseInt(code, 10) == 0)
        return false;
    code = ('0000' + code).substr(L + 4 - 10);
    if (parseInt(code.substr(3, 6), 10) == 0)
        return false;
    var c = parseInt(code.substr(9, 1), 10), s = 0;
    for (var i = 0; i < 9; i++)
        s += parseInt(code.substr(i, 1), 10) * (10 - i);
    s = s % 11;
    return (s < 2 && c == s) || (s >= 2 && c == (11 - s));
}
$.validator.addMethod("regxNationalcode", function (value) {
    
    return checkNationalCode(value);
});
$.validator.addMethod("regxDateFormat", function (value) {
    //Accepted format: 04 Nov 2015
    var regEx = /(?:1[234]\d{2})(\/|\-)(?:0?[1-9]|1[0-2])(\/|\-)(?:0?[1-9]|[12][0-9]|3[01])$/;//new RegExp("([0-2]{1}[0-9]{1}|3[0-1]{1})[ ][a-zA-Z]{3}[ ][0-9]{4}");
    // var regEx = new RegExp("([0-9]\d{1,}[-|/][a-zA-Z][-|/][0-9]\d{2,})");
    if (!value.length)
        return true;
    else if (!regEx.test(value))
        return false;
    else if (regEx.test(value))
        return true;

});

$.validator.addMethod(
    "regex",
    function (value, element, regexp) {
        if (!$(element).hasClass("disabledEL")) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        }
        return true;
    },
    "Please check your input."
    );

$.validator.addMethod("regexNumber", function (value) {
    var regEx = /^[0-9]+$/;
    if (!value.length)
        return true;
    else if (!regEx.test(value))
        return false;
    else if (regEx.test(value))
        return true;
});

$.validator.addMethod("regexFarsi", function (value) {

    var regEx = /^[\u0600-\u06FF\uFB8A\u067E\u0686\u06AF_ ]*$/;
    if (!value.length)
        return true;
    else if (!regEx.test(value))
        return false;
    else if (regEx.test(value))
        return true;
});


$.validator.addMethod("regexFarsiDesc", function (value) {

    var regEx = /^[\u0600-\u06FF\uFB8A\u067E\u0686\-\u06AF_ ]*$/;
    if (!value.length)
        return true;
    else if (!regEx.test(value))
        return false;
    else if (regEx.test(value))
        return true;
});



$.validator.addMethod("regexEnglish", function (value, element) {
    var regEx = /^[A-Za-z_ ]*$/;
    if (!value.length)
        return true;
    else if (!regEx.test(value))
        return false;
    else if (regEx.test(value))
        return true;
});


$.validator.addMethod("regexLetters", function (value, element) {
    // var regEx = /^[A-Za-z_ ]*$/;
    var regEx = /^[a-zA-Z0-9,.!? ]*$/;
    var regExFa = /^[\u0600-\u06FF\uFB8A\u067E\u0686\u06AF ]+$/;
    if (!value.length)
        return true;
    else if (!regEx.test(value) || !regExFa.test(value))
        return false;
    else
        return true;
});



$.validator.addMethod("regexPhone", function (value) {
    var regEx = /^[0-9]\d{3,}$/;
    if (!value.length)
        return true;
    else if (!regEx.test(value))
        return false;
    else if (regEx.test(value))
        return true;
});

$.validator.addMethod("regexMobile", function (value) {
    // var regEx = /^[09]\d{9,}$/;
    var regEx = /^(09)\d{8,}$/;
    if (!value.length)
        return true;
    else if (!regEx.test(value))
        return false;
    else if (regEx.test(value))
        return true;
});
$.validator.addMethod("regexPassport", function (value) {
    var regEx = /^[a-zA-Z]{1,3}\d{6,10}$/;
    if (!value.length)
        return true;
    else if (!regEx.test(value))
        return false;
    else if (regEx.test(value))
        return true;
});

$.validator.addMethod("regxDateFormat", function (value) {
    //Accepted format: 04 Nov 2015
    var regEx = new RegExp("([0-2]{1}[0-9]{1}|3[0-1]{1})[ ][a-zA-Z]{3}[ ][0-9]{4}");
    // var regEx = new RegExp("([0-9]\d{1,}[-|/][a-zA-Z][-|/][0-9]\d{2,})");
    if (!value.length)
        return true;
    else if (!regEx.test(value))
        return false;
    else if (regEx.test(value))
        return true;

});

$.validator.addMethod("regxGeneralDate", function (value) {
    var regEx = new RegExp("([0-9]{4}[-|/| ](0[1-9]|1[0-2])|([a-zA-Z])[-|/| ]([0-2]{1}[0-9]{1}|3[0-1]{1})|([0-2]{1}[0-9]{1}|3[0-1]{1})[-|/| ](0[1-9]|1[0-2])|([a-zA-Z])[-|/| ][0-9]{4})");
    if (!value.length)
        return true;
    else if (!regEx.test(value))
        return false;
    else if (regEx.test(value))
        return true;

});

//mzi
function checkBirthDay(value) {

    
    var birthDay = value;
    //get date now and convert to jalali
    var datenow = new Date();    
    datenow = toPersianDate(datenow);
    //convert date to day
    var yresultOfDateNow = (datenow.year * 365) + (datenow.month * 30.4167) + (datenow.date);
    //calculate 18 years ago
    var resultOfYear = yresultOfDateNow - (365 * 18);
    //convert date to day that user intered
    var dayOfDate = parseInt(birthDay.substring(8, 10));
    var monthOfDate = parseInt(birthDay.substring(5, 7));
    var yearOfDate = parseInt(birthDay.substring(0, 4));
    var ConvertDateToDay = ((parseInt(yearOfDate) * 365) + (parseInt(monthOfDate) * 30.4167) + parseInt(dayOfDate));
   
   

    if (resultOfYear < ConvertDateToDay) {
        return false;
    } else {
        return true;
    }
}


// for calculate age and validation for it
$.validator.addMethod("regexBirthDay", function (value) {
    
    return checkBirthDay(value);
});
