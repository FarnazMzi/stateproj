﻿
function showNotify(icon, message, type) {
    var notify = $.notify('<i class="' + icon + '"> </i><strong>' + message + '</strong> ', {
        type: type,
        showProgressbar: false,
        delay: 3000,
        placement: {
            from: "bottom",
            align: "left"
        },
        z_index: 1000000
    });
}

function showModal(url) {
  
    var modalContent = $("#FormModal .modal-content");
    // show loading
    $.ajax({
        url: url,
        type: 'GET',
        dataType: "html",
        success: function (data) {
            modalContent.html(data);
            $("#FormModal").modal('show');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //hide loading();
            showNotify('far fa-frown alert-icon', "!خطا در سیستم", 'error');
        }
    });
}


function LoadingAllPage() {
    $("body").append("<div id='LoadingAll' class='modalLoadingall'><div class='modal-contentLodingall' id='loadingcontent'><img  width='64' height='64' src='/img/ajaxLoaderCircle.gif' /></div></div>");
    // Get the modal
    var modal = document.getElementById('LoadingAll');
    modal.style.display = "block";
}
function LoadingAllPageOff() {
    // Get the modal
    var modal = document.getElementById('LoadingAll');
    modal.style.display = "none";
}

function CheckNationalCode(val) {
    if (val.length == 10) {
        var sumdata = 0;
        var j = 10;
        for (var i = 0; i <= 8; i++) {
            sumdata = sumdata + (parseInt(val[i]) * j);
            j--;
        }
        if (sumdata % 11 < 2 && sumdata % 11 == parseInt(val[9])) {
            return "1";
        } else if (parseInt(val[9]) == 11 - (sumdata % 11)) {
            return "1";
        } else {
            return "0";
        }
    } else {
        return "0";
    }
}

$(document).on("input", function (e) {
    $(".justpersian").bind('input propertychange', function (e) {
        if (!/^[ژپضصثقفغعهخحجچپگکمنتالبیسشظطزرذدئوآو\s\n\r\d\t\(\)\[\]\{\}.,،;\-؛]+$/.test($(this).val())) {
            var str = $(this).val();
            var i = str.length;
            i = i - 1;
            var res = str.slice(0, i);
            $(this).val(res);
        }
    });
});

$(document).on("input", function (e) {
    $(".justenglish").bind('input propertychange', function (e) {
        if (!/\d|\w|[\.\$\*\\\/\+\-\^\!\(\)\[\]\~\%\&\=\?\>\<\{\}\"\'\,\:\;\_]/.test($(this).val())) {
            var str = $(this).val();
            var i = str.length;
            i = i - 1;
            var res = str.slice(0, i);
            $(this).val(res);
        }
    });
});


var check = true;
$(document).on('click', '#btnSubmit, .btn-submit', function () {

    var form = $(this).closest('form');

    var validationStatus = formValidation($(this));
    var url = form.attr('action');
    var data = form.serialize();
    if (validationStatus && check ) {
        
        $.ajax({
            url: url,
            data: data,
            type: 'POST',
            beforeSend: function (data) {

                LoadingAllPage();
            },
            success: function (data) {
                
                LoadingAllPageOff();
                
                if (data == true) {
                    
                    var modalContent = $("#FormModal .modal-content");
                    modalContent.html(showSuccessMessage);
                    
                }
                else
                    showNotify('far fa-frown alert-icon',data.messageString, 'error')
            },
            error: function (xhr, ajaxOptions, thrownError) {

                LoadingAllPageOff();

                var exceptionMessage = messageParse(xhr.responseText);
                if (exceptionMessage != null)
                    showNotify('far fa-frown alert-icon', data.messageString, 'error')
                else
                    showNotify('far fa-frown alert-icon',"!عملیات با خطا مواجه شد", 'error');
            }
        });
    }

})

$(document).ready(function () {


    $('.key-active').keypress(function (event) {
        if (event.keyCode == 13) {
            $('.key-press').click();
        }
    });

    //for convert to number
    $('.nu').keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });


});

function showSuccessMessage() {
    var str = '<div class="modal-header">';        
    str += '<div class="col text-center">' + '<span style="display:inline-block" class="mt5 text-center"> گزارش تایید </span> </div></div>';
    str += '<div class="modal-body text-center"><br /> <i class="fas" style="color:green; font-size:70px ; font-weight:900;">&#xf00c;</i>';
    str += '<br /><br /><br /><span> عملیات با موفقیت انجام شد</span></div>';
    str += '<div class="modal-footer text-center"><div class="col"><button type="button" class="btn btnGreenColorStyle " onclick="location.reload(true)" data-dismiss="modal" style="width:125px">تایید</button></div></div>';
    return str;
}


$(document).on('click', '#btn-search', function () {
    var dataTable = $('.data-grid');
    if (dataTable.length > 0) {
        var table = dataTable.DataTable();
        table.ajax.reload(null, true);
    }
});
function leaveInput(el) {
    if (el.value.length > 0) {
        if (!el.classList.contains('active')) {
            el.classList.add('active');
        }
    } else {
        if (el.classList.contains('active')) {
            el.classList.remove('active');
        }
    }
}

var inputs = document.getElementsByClassName("m-input");
for (var i = 0; i < inputs.length; i++) {
    var el = inputs[i];
    el.addEventListener("blur", function () {
        leaveInput(this);
    });
}




function formValidation(elemnt) {
    var me = $(elemnt);
    var form = me.closest('form');
    var validate = true;
    
    form.find(':input[data-addV]:visible').each(function () {
        
        var currentItem = $(this);
        var value = currentItem.val().trim();

        if (value == null || value == '') {
            validate = false;

            // add class red validate
            currentItem.closest('.invalid').addClass('validationRed');
            currentItem.attr('placeholder', 'مقدار را وارد نمایید');
        }
        else {
            // remove class red validate
            currentItem.closest('.invalid').removeClass("validationRed");
            
            }
     
    }); 

    return validate;

}

var getCity = false;
function getBaseInfo(groupId, parentId)
{
    $.ajax({
        type: "GET",
        url: "/BaseInfo/BaseInfoSelection/",
        data: { groupId, parentId },
        success: function (data) {
            var s = ' <option value="" style="padding-left:20px">انتخاب کنید</option>';
            for (var i = 0; i < data.length; i++) {
                s += '<option value="' + data[i].id + '">' + data[i].lookUpName + '</option>';
            }

            $.each(data, function () {

                if ($('#GroupId').val() != "" && getCity == false) {
                    $(".parent").html(s);
                }
                if (getCity) {
                    $(".city").html(s);
                }

            });
        }
    });
}



//currentItem.closest('.rowStyle').addClass('validationRed').after('<span class="text-invalid">مقدار را وارد نمایید</span>');
//$('.text-invalid').remove();

