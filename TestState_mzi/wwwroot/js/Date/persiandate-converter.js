﻿// converter date time to perisan date , ...

function TimeStampToDateTime(timeStamp) {
    var date = timeStamp.toString().substring(0, 10);
    var dateTime = new Date(date * 1000);
    var month = dateTime.getMonth() + 1;
    var standardDate = dateTime.getFullYear() + "/" + month + "/" + dateTime.getDate();
    return standardDate;
}

function GregorianToPersianDate(gregorianDate) {
    if (gregorianDate != null) {

        // convert miladi date to persian date by jalali function
        var miladiDate = gregorianDate.toString().split("/");
        var persianDate = JalaliDate.gregorianToJalali(miladiDate[2], miladiDate[1], miladiDate[0]);
        return persianDate;
    }
}
function PersianToGregorianDate(perisanDate) {
    
    // convert persian date to miladi date by jalali function
    var shamsiDate = perisanDate.split("/");
    var gregorianDate = JalaliDate.jalaliToGregorian(shamsiDate[0], shamsiDate[1], shamsiDate[2]);
    return gregorianDate;
}

function GregorianToPersianDateTime(gregorianDateTime) {
    // convert miladi dateTime to persian dateTime by jalali function
    var gregorianDate = gregorianDateTime.split(" ");
    var miladiDate = gregorianDate[0].split("/");
    var persianDateTime = JalaliDate.gregorianToJalali(miladiDate[0], miladiDate[1], miladiDate[2]) + " " + gregorianDate[1];
    return persianDateTime;
}

function GetPersianDateName(date, withYear, withDayofWeekName) {
    var persianDate = GregorianToPersianDate(date).split("/");
    if (withYear) {
        if (withDayofWeekName)
            return dayofWeekPersianName(getDayofWeek(date)) + " " + persianDate[2] + " " + getPersianMonthName(persianDate[1]) + " " + persianDate[0];
        else
            return persianDate[2] + " " + getPersianMonthName(persianDate[1]) + " " + persianDate[0];
    }
    else {
        if (withDayofWeekName)
            return dayofWeekPersianName(getDayofWeek(date)) + " " + +persianDate[2] + " " + getPersianMonthName(persianDate[1]);
        else
            return persianDate[2] + " " + getPersianMonthName(persianDate[1]);
    }
}

// get time only
var getTimeNow = function (withSecond) {
    var today = new Date();
    var hour = today.getHours();
    var min = today.getMinutes();
    var sec = today.getSeconds();

    if (min < 10) { min = '0' + min; }
    if (sec < 10) { sec = '0' + sec; }

    if (withSecond)
        return nowTime = hour + ':' + min + ":" + sec;
    else
        return nowTime = hour + ':' + min;

}
// get date only
var getDateNow = function () {
    var today = new Date();
    var day = today.getDate();
    var month = today.getMonth() + 1; //January is 0!
    var year = today.getFullYear();
    return nowDay = year + '/' + month + '/' + day;
}

var getDayofWeek = function (date) {
    var selectedDate = new Date(date);
    var dayOfWeek = selectedDate.getDay();
    return dayOfWeek;
}
var diffrenceTimeToNow = function (startDateTime) {
    var now = new Date();
    return diffrenceTwoDateTime(startDateTime, now);

}

var diffrenceTwoDateTime = function (startDateTime, endDateTime) {
    var timeDiff = endDateTime.getTime() - startDateTime.getTime();
    var diffYears = timeDiff / (1000 * 3600 * 24 * 30 * 12);
    var diffMonths = timeDiff / (1000 * 3600 * 24 * 30);
    var diffWeeks = timeDiff / (1000 * 3600 * 24 * 7);
    var diffDays = timeDiff / (1000 * 3600 * 24);
    var diffHours = timeDiff / (1000 * 3600);
    var diffMin = timeDiff / (60000);

    // calculate time
    if (diffYears >= 1)
        return Math.floor(diffYears) + " سال ";
    else if (diffMonths >= 1)
        return Math.floor(diffMonths) + " ماه ";
    else if (diffWeeks >= 1)
        return Math.floor(diffWeeks) + " هفته ";
    else if (diffDays >= 1)
        return Math.floor(diffDays) + " روز ";
    else if (diffHours >= 1)
        return Math.floor(diffHours) + " ساعت ";
    else if (diffMin >= 1)
        return Math.floor(diffMin) + " دقیقه ";
    else
        return " لحظاتی ";

    var data = {

    }
}
var dayofWeekPersianName = function (dayOfWeek) {
    switch (dayOfWeek) {
        case 0:
            return "یک شنبه";
        case 1:
            return "دوشنبه";
        case 2:
            return "سه شنبه";
        case 3:
            return "چهارشنبه";
        case 4:
            return "پنج شنبه";
        case 5:
            return "جمعه";
        case 6:
            return "شنبه";
    };
}
var getPersianMonthName = function (month) {
    switch (month) {
        case "1":
            return "فروردین";
        case "2":
            return "اردیبهشت";
        case "3":
            return "خرداد";
        case "4":
            return "تیر";
        case "5":
            return "مرداد";
        case "6":
            return "شهریور";
        case "7":
            return "مهر";
        case "8":
            return "آبان";
        case "9":
            return "آذر";
        case "10":
            return "دی";
        case "11":
            return "بهمن";
        case "12":
            return "اسفند";
    }
}

var dayofWeekMiladiName = function (dayOfWeek) {
    switch (dayOfWeek) {
        case 0:
            return "Sunday";
        case 1:
            return "Monday";
        case 2:
            return "Tuesday";
        case 3:
            return "Wednesday";
        case 4:
            return "Thursday";
        case 5:
            return "Friday";
        case 6:
            return "Satuday";
    };
}
var getMiladiMonthName = function (month) {
    switch (month) {
        case "1":
            return "January";
        case "2":
            return "February";
        case "3":
            return "March";
        case "4":
            return "April";
        case "5":
            return "May";
        case "6":
            return "June";
        case "7":
            return "July";
        case "8":
            return "August";
        case "9":
            return "September";
        case "10":
            return "October";
        case "11":
            return "November";
        case "12":
            return "December";
    }
}
var JalaliDate = {
    g_days_in_month: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
    j_days_in_month: [31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29]
};

JalaliDate.jalaliToGregorian = function (j_y, j_m, j_d) {
    j_y = parseInt(j_y);
    j_m = parseInt(j_m);
    j_d = parseInt(j_d);
    var jy = j_y - 979;
    var jm = j_m - 1;
    var jd = j_d - 1;

    var j_day_no = 365 * jy + parseInt(jy / 33) * 8 + parseInt((jy % 33 + 3) / 4);
    for (var i = 0; i < jm; ++i) j_day_no += JalaliDate.j_days_in_month[i];

    j_day_no += jd;

    var g_day_no = j_day_no + 79;

    var gy = 1600 + 400 * parseInt(g_day_no / 146097); /* 146097 = 365*400 + 400/4 - 400/100 + 400/400 */
    g_day_no = g_day_no % 146097;

    var leap = true;
    if (g_day_no >= 36525) /* 36525 = 365*100 + 100/4 */ {
        g_day_no--;
        gy += 100 * parseInt(g_day_no / 36524); /* 36524 = 365*100 + 100/4 - 100/100 */
        g_day_no = g_day_no % 36524;

        if (g_day_no >= 365)
            g_day_no++;
        else
            leap = false;
    }

    gy += 4 * parseInt(g_day_no / 1461); /* 1461 = 365*4 + 4/4 */
    g_day_no %= 1461;

    if (g_day_no >= 366) {
        leap = false;

        g_day_no--;
        gy += parseInt(g_day_no / 365);
        g_day_no = g_day_no % 365;
    }

    for (var i = 0; g_day_no >= JalaliDate.g_days_in_month[i] + (i == 1 && leap); i++)
        g_day_no -= JalaliDate.g_days_in_month[i] + (i == 1 && leap);
    var gm = i + 1;
    var gd = g_day_no + 1;

    var gregorianDate = gy + '/' + gm + '/' + gd;

    return gregorianDate;
}

JalaliDate.gregorianToJalali = function (g_y, g_m, g_d) {
    g_y = parseInt(g_y);
    g_m = parseInt(g_m);
    g_d = parseInt(g_d);
    var gy = g_y - 1600;
    var gm = g_m - 1;
    var gd = g_d - 1;

    var g_day_no = 365 * gy + parseInt((gy + 3) / 4) - parseInt((gy + 99) / 100) + parseInt((gy + 399) / 400);

    for (var i = 0; i < gm; ++i)
        g_day_no += JalaliDate.g_days_in_month[i];
    if (gm > 1 && ((gy % 4 == 0 && gy % 100 != 0) || (gy % 400 == 0)))
        /* leap and after Feb */
        ++g_day_no;
    g_day_no += gd;

    var j_day_no = g_day_no - 79;

    var j_np = parseInt(j_day_no / 12053);
    j_day_no %= 12053;

    var jy = 979 + 33 * j_np + 4 * parseInt(j_day_no / 1461);

    j_day_no %= 1461;

    if (j_day_no >= 366) {
        jy += parseInt((j_day_no - 1) / 365);
        j_day_no = (j_day_no - 1) % 365;
    }

    for (var i = 0; i < 11 && j_day_no >= JalaliDate.j_days_in_month[i]; ++i) {
        j_day_no -= JalaliDate.j_days_in_month[i];
    }
    var jm = i + 1;
    var jd = j_day_no + 1;

    var persianDate = jy + '/' + jm + '/' + jd;
    return persianDate;
}
//JalaliDate.gregorianToJalali = function (g_y, g_m, g_d) {
//    g_y = parseInt(g_y);
//    g_m = parseInt(g_m);
//    g_d = parseInt(g_d);
//    var gy = g_y - 1600;
//    var gm = g_m - 1;
//    var gd = g_d - 1;

//    var g_day_no = 365 * gy + parseInt((gy + 3) / 4) - parseInt((gy + 99) / 100) + parseInt((gy + 399) / 400);

//    for (var i = 0; i < gm; ++i)
//        g_day_no += JalaliDate.g_days_in_month[i];
//    if (gm > 1 && ((gy % 4 == 0 && gy % 100 != 0) || (gy % 400 == 0)))
//        /* leap and after Feb */
//        ++g_day_no;
//    g_day_no += gd;

//    var j_day_no = g_day_no - 79;

//    var j_np = parseInt(j_day_no / 12053);
//    j_day_no %= 12053;

//    var jy = 979 + 33 * j_np + 4 * parseInt(j_day_no / 1461);

//    j_day_no %= 1461;

//    if (j_day_no >= 366) {
//        jy += parseInt((j_day_no - 1) / 365);
//        j_day_no = (j_day_no - 1) % 365;
//    }

//    for (var i = 0; i < 11 && j_day_no >= JalaliDate.j_days_in_month[i]; ++i) {
//        j_day_no -= JalaliDate.j_days_in_month[i];
//    }
//    var jm = i + 1;
//    var jd = j_day_no + 1;

//    var persianDate = jy + '/' + jm + '/' + jd;
//    return persianDate;
//}