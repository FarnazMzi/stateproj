﻿using Microsoft.EntityFrameworkCore;
using TestState_mzi.Models;

namespace TestState_mzi.Data
{
    public class StateContext : DbContext
    {
        public StateContext(DbContextOptions<StateContext> options) 
            : base(options)
        {
        }

        public DbSet<LandOwnerModel> LandOwners { get; set; }
        public DbSet<LandModel> Lands { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LandOwnerModel>(b =>
            {
                b.HasKey(e => e.Id);
                b.Property(e => e.Id).ValueGeneratedOnAdd();
                b.ToTable("Land");
            });
            modelBuilder.Entity<LandOwnerModel>(b =>
            {
                b.HasKey(e => e.Id);
                b.Property(e => e.Id).ValueGeneratedOnAdd();
                b.ToTable("LandOwner");
            });
           
        }
    }
}
