﻿using TestState_mzi.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestState_mzi.Data;

namespace TestState_mzi.Data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        public StateContext _Context { get; }
        public UnitOfWork(StateContext Context)
        {
            _Context = Context;
        }

        public IRepositoryBase<TEntity> BaseRepository<TEntity>() where TEntity : class
        {
            IRepositoryBase<TEntity> repository = new RepositoryBase<TEntity, StateContext>(_Context);
            return repository;
        }


        public async Task Commit()
        {
            await _Context.SaveChangesAsync();
        }
    }
}
