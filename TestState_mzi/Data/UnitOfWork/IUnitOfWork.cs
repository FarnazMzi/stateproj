﻿using TestState_mzi.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestState_mzi.Data;

namespace TestState_mzi.Data.UnitOfWork
{
    public interface IUnitOfWork
    {
        StateContext _Context { get; }
        IRepositoryBase<TEntity> BaseRepository<TEntity>() where TEntity : class;
        Task Commit();
    }
}
