﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestState_mzi.Models;

namespace TestState_mzi.Data
{
    public static class DbInitializer
    {
        public static void Initialize(StateContext context)
        {
            context.Database.EnsureCreated();


            if (context.Lands.Any())
            {
                return;   // DB has been seeded
            }

            var landOwner = new LandOwnerModel { FirstName = "farnaz", LastName = "mzi", PhoneNumber = "09109733900" };
            context.LandOwners.Add(landOwner);
            context.SaveChanges();

            var lands = new LandModel[]
            {
            new LandModel{LandNumber=1000006,Name="اداری",Dimension=1000,Address="میرداماد ، خ نفت شمالی",IsNortherly = true,LandOwnerId=1,IsDelete=false,Log="Default"},
            new LandModel{LandNumber=1000009,Name="تجاری",Dimension=500,Address="پیروزی ، بلوار ابوذر",IsNortherly = false,LandOwnerId=1,IsDelete=false,Log="Default"},
            };
            foreach (var land in lands)
            {
                context.Lands.Add(land);
            }
            context.SaveChanges();
           
        }
    }
}
