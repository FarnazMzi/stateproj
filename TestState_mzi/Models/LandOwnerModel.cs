﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TestState_mzi.Models
{
    public class LandOwnerModel
    {      
        public int Id { get; set; }
        [StringLength(30, MinimumLength = 5)]
        public string FirstName { get; set; }
        [StringLength(50, MinimumLength = 5)]
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
    }
}
