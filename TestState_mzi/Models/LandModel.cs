﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TestState_mzi.Models
{
    public class LandModel
    {
        public int Id { get; set; }
        [Required]
        public int LandNumber { get; set; }
        [StringLength(30, MinimumLength = 5)]
        public string Name { get; set; }
        [Required]
        public int Dimension { get; set; }
        [StringLength(150)]
        [Required]
        public string Address { get; set; }
        public bool IsNortherly { get; set; }
        [Required]
        public int LandOwnerId { get; set; }
        [StringLength(150)]
        [Required]
        public string Log { get; set; }
        public bool IsDelete { get; set; }
    }
}
