﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestState_mzi.ViewModels;

namespace TestState_mzi.Controllers
{
    public class AccountController : Controller
    {
        public IActionResult Login()
         => View();

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login(LoginViewModel login)
        {
            if (login.UserName == "fmzi" && login.PhoneNumber == "09109733900" || login.UserName == "nilii" && login.PhoneNumber == "09195965689")
            {
                HttpContext.Session.SetString("UserName", login.UserName);

                return Redirect("~/Land/Index");
            }
            return View(login.StatusText = "اطلاعات وارد شده صحیح نمیباشد");
        }
    }
}
