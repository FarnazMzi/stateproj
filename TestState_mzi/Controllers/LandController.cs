﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TestState_mzi.Models;
using TestState_mzi.Data.UnitOfWork;
using TestState_mzi.Tools;

namespace TestState_mzi.Controllers
{
    public class LandController : Controller
    {
        private ILogger<LandController> _logger;
        private readonly IUnitOfWork _uW;
        public LandController(IUnitOfWork uW , ILogger<LandController> logger)
        {
            _logger = logger;
            _uW = uW;
        }
        public async Task<IActionResult> Index()
        {
            List<LandModel> result = await LandManagement();
            return View(result);
        }

        [HttpPost]
        public async Task<List<LandModel>> LandManagement()
        {
            try
            {
                var lands = await _uW.BaseRepository<LandModel>().FindByConditionAsync(x => x.IsDelete == false);               

                return lands.ToList();
            }
            catch (Exception exp)
            {
                return null;
            }
        }

        [HttpPost]
        public async Task<LandModel> LandGetById(int id)
        {
            try
            {
                var land = await _uW.BaseRepository<LandModel>().FindByID(id);
                return land;
            }
            catch (Exception exp)
            {
                return null;
            }
        }
        public ActionResult CreateLand()
            => View();


        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<object> CreateLand(LandModel model)
        {      
            try
            {
                model.Log = LogString.ReturnLog(HttpContext.Session,"Create");
                var result = _uW.BaseRepository<LandModel>().Create(model);
                await _uW.Commit();

                return true;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp.Message);
                return false; 
            }

        }
        [HttpGet]
        public async Task<ActionResult> UpdateLand(int id)
        {
            LandModel result = await LandGetById(id);
            return View(result);
        }

        [HttpPost]
        public object UpdateLand(LandModel model)
        { 
            try
            {
                model.Log = LogString.ReturnLog(HttpContext.Session, "Update");
                _uW.BaseRepository<LandModel>().Update(model);
                var result = _uW.Commit();

                return true;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp.Message);
                return false; 
            }
        }

        [HttpGet]
        public async Task<object> DeleteLand(int id)
        {
            try
            {
                LandModel model = await LandGetById(id);
                model.Log = LogString.ReturnLog(HttpContext.Session, "Delete");
                model.IsDelete = true;

                _uW.BaseRepository<LandModel>().Update(model);
                await _uW.Commit();
                return true;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp.Message);
                return false;
            }
        }

    }
}
